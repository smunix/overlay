# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit git-2 eutils 

DESCRIPTION="LLVM D Compiler"
HOMEPAGE="http://www.dsource.org/projects/ldc"

EGIT_REPO_URI="git://github.com/ldc-developers/ldc.git"

LICENSE="BSD"
SLOT="2"
KEYWORDS="~x86 ~amd64 ~ppc ~sparc"
IUSE=""
EAPI="2"

RESTRICT="mirror"

RDEPEND=">=sys-devel/llvm-2.6
		|| ( dev-libs/libelf dev-libs/elfutils )"
DEPEND=">=dev-util/cmake-2.6-r1
		dev-libs/libconfig
		${RDEPEND}"

S="${WORKDIR}/${PN}"

src_prepare() {
	git submodule update -i
}

src_configure() {
	cd "${S}"
	cmake -DCMAKE_CXX_FLAGS:STRING="-DLLVM_REV=999999" -DSYSCONF_INSTALL_DIR:PATH="${D}etc" -DCMAKE_INSTALL_PREFIX="${D}usr" ./ ||
	die "cm  ake failed"
}

src_compile() {
	cd "${S}"
    emake || die "make failed"
}

src_install() {
	emake install || die "Install failed"
# The following failed to be done automagically. You will have to run it
# manually!
#
#	sed -e 's+'"${D}"'+'"${ROOT}"'+g' -i "${D}"etc/ldc2.conf
#	sed -e 's+'"${D}"'+'"${ROOT}"'+g' -i "${D}"etc/ldc2.rebuild.conf
#
# Here follows the manual way:
#	sed -e 's+/var/tmp/portage/dev-lang/ldc-9999-r1/image/+/+g' -i /etc/ldc2.*conf
}

